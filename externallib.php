<?php

require_ONCE($CFG->dirroot . '/config.php');
require_once($CFG->libdir . "/externallib.php");
require_once($CFG->libdir . "/accesslib.php");
require_once($CFG->dirroot . "/mod/quiz/lib.php");
require_once($CFG->libdir . "/gradelib.php");
require_once($CFG->dirroot . "/config.php");

class local_quizattempts_external extends external_api {

/**
 * Tell the Moodle webservices what parametes the quiz_get_attempts function will accept.
 * 
 * @return external_function_parameters Instance of the external_function_paramters class
**/

	public static function quiz_get_attempts_parameters() {
		return new external_function_parameters(
			array(
				'qid' => new external_value(PARAM_INT, 'Quiz id', VALUE_DEFAULT, null),
				'modid' => new external_value(PARAM_INT, 'Module id', VALUE_DEFAULT, null),
				'from' => new external_value(PARAM_INT, 'Beginning date rage (unix time)', VALUE_DEFAULT, null),
				'to' => new external_value(PARAM_INT, 'End date range (unix time)', VALUE_DEFAULT, null),
				'username' => new external_value(PARAM_ALPHANUMEXT, 'NetID', VALUE_DEFAULT, null),
				'userid' => new external_value(PARAM_INT, 'User id', VALUE_DEFAULT, null)
			));
	}

/**
 * This function fetches and returns the quiz attempts.
 * Either a quiz id or a module id is required.  We allow the module id
 * option since the module id is easly viewable when one visists 
 * /mod/quiz/view.php?id=X.  See mod/quiz/view.php for more information.  
 * 
 * Required input: $qid or $modid. If both are provided, only the $qid is used.
 * Optional input: 	$from, $to, $username, $userid
 *  - $from and $to will only return records where $from < timemodified < $to.
 *  - If both $username and $userid are provided, only the $username is used.
 *	
 * @param int		$qid	Quiz id from mdl_quiz.id, defaults to null
 * @param int		$modid	Module id corresponding to the quiz instance defaults to null
 * @param int		$from	Unix timestamp indicating the beginning time range for the attempts 
 * @param int		$to		Unix timestamp indicating the end time range for the attempts
 * @param string	$username	A student's NetID.  We make it a PARAM_ALPHANUMEXT to allow for _ and -
 * @param int		$userid	A moodle id for a student (mdl_user.id)
 * 
 * @return	array	An array of attempts
 * @throws	moodle_exception
 */
	public static function quiz_get_attempts($qid, $modid, $from, $to, $username, $userid) {
		global $DB, $USER;

		// Standard Moodle external webservice parameter validation.
		// The returned array contains sanitized parameters

		$params = self::validate_parameters(self::quiz_get_attempts_parameters(),
			array('qid' => $qid, 'modid' => $modid,
					'from' => $from, 'to' => $to, 'username' => $username,
					'userid' => $userid));
		

		/**
		 * Get the correct course for the quiz based on either the quiz id or the module id
		 * 
		 * Taken from mod/quiz/view.php
		 * We need to get the course id for course in which the quiz resides so that we can get the correct 
		 * context for looking up students in the course using the get_role_users() function.
		 */

		// If we're given a quiz id (mdl_quiz.id), we only need to find the course id.
		if ($params['qid'] !== null) { 
			if (!$quiz = $DB->get_record('quiz', array('id' => $params['qid']))) {
				add_to_log(get_site()->id, 'quizattempts', 'fetch', '',  'fail: invalid quizid '. $params['qid'], '', $USER->id); 
				throw new moodle_exception("Cannot find quiz id" . $params['qid']);
			}

			if (!$course = $DB->get_record('course', array('id' => $quiz->course))) {
				add_to_log(get_site()->id, 'quizattempts', 'fetch', '', 'fail: no course with quiz id ' . $quiz->id, '', $USER->id); 
				throw new moodle_exception("Cannot find a course associated to the quiz id");
			}
		} else { // If the quiz id is given, the modid is ignored! 
			// Without the quiz id we must have a module id, otherwise we have no way of knowing what quiz the user wants
			if ($params['modid'] === null) { 
				add_to_log(get_site()->id, 'quizattempts', 'fetch', '', 'fail: no modid or qid', '', $USER->id); 
				throw new moodle_exception("Need a quiz id OR a module id");
			} else {
				if (!$cm = get_coursemodule_from_id('quiz', $params['modid'])) {
					add_to_log(get_site()->id, 'quizattempts', 'fetch', '', 'fail invalid module id '. $params['modid'], '', $USER->id); 
					throw new moodle_exception("Invalid module id");
				}
				if (!$course = $DB->get_record('course', array('id' => $cm->course))) {
					add_to_log(get_site()->id, 'quizattempts', 'fetch', '',  'fail: no course for modid ' . $params['modid'], '', $USER->id); 
					throw new moodle_exception("Cannot find a course associated to the module id");
				}
				$quiz = new stdClass();
				$quiz->id = $cm->instance;
			}
		}

		$context = get_context_instance(CONTEXT_COURSE, $course->id); 	// Get the context for the course based on the course id

		/**
		 * Security Checks
		 */

		self::validate_context($context);	// Make sure that the user may execute the function in the course context
		if (!has_capability('mod/quiz:viewreports', $context)) {	 // We require that the user is able to view reports.

			add_to_log(get_site()->id, 'quizattempts', 'fetch', '../mod/quiz/view.php?qid=' . $quiz->id, 'fail: permission denied', $context->id, $USER->id); 
			throw new moodle_exception("Permission denied.");
		}
		/**
		 * Obtain a list of students.
		 * If $username is given, return only entries for $username. Ignore everything else.
		 * If $userid is given, return only entries for $userid.
		 * Otherwise, use get_role_users to get all users with the student role from the course
		 */

		$students = array(); // collect the students we want to look up

		if ($params['username'] !== null) { // get student by username
			$student = $DB->get_record('user', array('username' =>
													$params['username']));
			
			if (!$student) { // Check to see if we found a student.  Throw an exception if not.
				add_to_log(get_site()->id, 'quizattempts', 'fetch', '../mod/quiz/view.php?qid=' . $quiz->id, 'fail: invalid user ' . $params['username'], $context->id, $USER->id); 
				throw moodle_exception("Cannot find a student with the username " . $params['username']);
			}

			$students[] = $student;
		} else if ($params['userid'] !== null) { // get student by id
			$student = $DB->get_record('user', array('id' =>
													$params['userid']));

			if (!$student) { // Check to see if we found a student. Throw an exception if not.
				add_to_log(get_site()->id, 'quizattempts', 'fetch', '../mod/quiz/view.php?qid=' . $quiz->id, 'fail: invalid user ' . $params['userid'], $context->id, $USER->id); 
				throw moodle_exception("Cannot find a student with the userid " . $params['userid']);
			}

			$students[] = $student;
		} else { // get all students
			$fields = 'u.username, u.id, u.phone2'; // we only care about the username,  user id, and phone2 (UIN)

			try { // Try to get the student role
				$studentrole = $DB->get_record('role', array('shortname' => 'student'), '*', MUST_EXIST); // get the role record for 'Student'
			} catch (Exception $e) {
				add_to_log(get_site()->id, 'quiz', 'fetch', 'view.php?q=' . $quiz->id, 'fail: no roleid for student ' . $params['userid'], $context->id, $USER->id); 
				throw moodle_exception("Unable to get a role id for the student role: " . $e->getMessage());
			}

			$students = get_role_users($studentrole->id, $context, false, $fields); // get all users in a course with role 'student'
		}

		$out = array();  // return array

		/**
		 * We allow two unix time stamps for filtering the records: from and to.
		 * we will only return records where $params['from'] < $attempt->timemodified < $params['to'].
		 * If $from is not set, we make it 0, so there's no lower bound for $attempt->timemodified.
		 * If $to is not set, we make it the current time, so there's no upper bound for $attempt->timemodified.
		*/

		if ($params['from'] === null) {
			$params['from'] = 0;
		}

		if ($params['to'] === null) {
			$params['to'] = mktime();
		}

		foreach ($students as $student) { // Loop through all the students in the course 
			$attempts = quiz_get_user_attempts($quiz->id, $student->id, 'finished'); 

			if (count($attempts) > 0) { // We have at least one attempt - fetch final grades from the gradebook
				$gb_gradeinfo = grade_get_grades($course->id, 'mod', 'quiz', $quiz->id, $student->id);
				// The next trick requires at least php 5.3!
				$finalgrade = $gb_gradeinfo->items[0]->grades[$student->id]->str_grade ?: "No grade in gradebook";
			}

			foreach ($attempts as $attempt) {
				if (($params['from'] > $attempt->timemodified) or 
						($params['to'] < $attempt->timemodified)) { // Verify that we're within the requested date range
					continue;										// by default that includes all records
				}
				$row = array();
				$row['username'] = $student->username;
				$row['UIN'] = $student->phone2 ?: 0;
				$row['attemptid'] = $attempt->id;
				$row['sumgrades'] = $attempt->sumgrades;
				$row['quiz'] = $quiz->id;
				$row['timemodified'] = $attempt->timemodified;
				$row['timefinish'] = $attempt->timefinish;
				$row['timestart'] = $attempt->timestart;
				$row['state'] = $attempt->state;
				$row['uniqueid'] = $attempt->uniqueid;
				$row['finalgrade'] = $finalgrade;
				$out[] = $row;
			}
		}
		
		add_to_log(get_site()->id, 'quiz', 'fetch quiz attempts', 'view.php?q=' .  $quiz->id, 'success', $context->id, $USER->id); 
		return $out;
	}

/**
 * REQUIRED: This function returns a structure that describes the return value
 * 
 * @returns	external_multiple_structure	return paramters
*/
	public static function quiz_get_attempts_returns() {
		return new external_multiple_structure(
			new external_single_structure(
					array(
						'username' => new external_value(PARAM_TEXT, 'text: username'),
						'UIN' => new external_value(PARAM_INT, 'int: UIN'),
						'sumgrades' => new external_value(PARAM_RAW, 'int: attempt sumgrades'),
						'quiz' => new external_value(PARAM_INT, 'int: quiz id'),
						'timemodified' => new external_value(PARAM_INT, 'int: attempt last modified time (unix)'),
						'timestart' => new external_value(PARAM_INT, 'int: attempt start time (unix)'),
						'timefinish' => new external_value(PARAM_INT, 'int: finished time (unix)'),
						'attemptid' => new external_value(PARAM_INT, 'int: attempt id'),
						'state' => new external_value(PARAM_TEXT, 'text: attempt state'),
						'uniqueid' => new external_value(PARAM_INT, 'int: attempt unique id'),
						'finalgrade' => new external_value(PARAM_RAW, 'mixed: final grade in the gradebook or error message'),
					)
				)
			);
	}
}

