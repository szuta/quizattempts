<?php

$functions = array(
	'local_quiz_get_attempts' => array(
		'classname' => 'local_quizattempts_external',
		'methodname' => 'quiz_get_attempts',
		'classpath' => 'local/quizattempts/externallib.php',
		'description' => 'Returns quiz attempts',
		'type' => 'read',
	)	
);

$services = array(
	'Quiz attempts web service' => array(
		'functions' => array('local_quiz_get_attempts'),
		'restrictedusers' => 0,
		'enabled' => 1,
	)
);
