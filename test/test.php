<?php
// Settings

/*
$serverURL = "https://sb04.atlas.illinois.edu/webservice/rest/server.php?";
$goodtoken = "a35f2ec1a8a90217374889954b5d54c5";
$badtoken = "f9fadefdff851998137cb39a630b6139";
$quiz1modid = 15;
$quiz2modid = 16;
$randomquizid = 1247145134;
*/

$serverURL = "https://dev.learn.illinois.edu/webservice/rest/server.php?";
$goodtoken = "c3f6c5538e66a9a82e804c9e8c10a19e";
$badtoken = "f9fadefdff851998137cb39a630b6139";
$quiz1modid = 65082;
$quiz2modid = 65083;
$randomquizid = 1341323;


function fetch_data($params) {
	global $serverURL, $goodtoken, $badtoken, $quiz1modid, $quiz2modid;

	$getparams = "wsfunction=local_quiz_get_attempts";
	foreach ($params as $key => $value) {
		$getparams .= "&{$key}=" . urlencode($value);
	}

	$ch = curl_init($serverURL.$getparams);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, null);
	curl_setopt($ch, CURLOPT_POST, false);
	curl_setopt($ch, CURLOPT_HTTPGET, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$output = curl_exec($ch);
	return $output;	
}

$tests = array(
	"Quiz 1 fetch with good token" => array(
		"params" => array(
			"wstoken" => $goodtoken,
			"modid" => $quiz1modid
		),
		"expected" => array(
			"//KEY[contains(@name,'username')]/VALUE"=> array("atlasmoodle4", "atlasmoodle4"),
			"//KEY[contains(@name,'finalgrade')]/VALUE"=> array("100.00", "100.00"),
		)
	),
	"Quiz 2 fetch with good token" => array(
		"params" => array(
			"wstoken" => $goodtoken,
			"modid" => $quiz2modid
		),
		"expected" => array(
			"//KEY[contains(@name,'username')]/VALUE"=> array("atlasmoodle5"),
			"//KEY[contains(@name,'finalgrade')]/VALUE"=> array("50.00"),
		)
	),
	"Quiz 1 fetch with bad token" => array(
		"params" => array(
			"wstoken" => $badtoken,
			"modid" => $quiz1modid
		),
		"expected" => array(
			"//ERRORCODE" => array("Permission denied."),
		)
	),
	"Quiz 2 fetch with bad token" => array(
		"params" => array(
			"wstoken" => $badtoken,
			"modid" => $quiz2modid
		),
		"expected" => array(
			"//ERRORCODE" => array("Permission denied."),
		)
	),
	"Random modid fetch" => array(
		"params" => array(
			"wstoken" => $badtoken,
			"modid" => $randomquizid
		),
		"expected" => array(
			"//ERRORCODE" => array("Invalid module id"),
		)
	),
);

foreach ($tests as $testname => $test) {
	echo $testname . "\n";

	$xml = new SimpleXMLElement(fetch_data($test["params"]));
	foreach ($test["expected"] as $key => $result) {
		$xpath = $xml->xpath($key);
		for ($i = 0; $i < count($result); $i++) {
			if (!array_key_exists($i, $xpath)) {
				echo "--- Failed: too few results from webservice\n";
			} else {
				if ($xpath[$i][0] == $result[$i]) {
					echo "--- Passed...\n";
				} else {
					echo "--- Failed: " . $xpath[$i][0] . " not equal to " . $result[$i] . "\n";
				}
			}
		}
	}
}
?>
